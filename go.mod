module gitlab.com/gitlab-org/release-cli

go 1.13

require (
	github.com/mitchellh/gox v1.0.1
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.4.0
	github.com/urfave/cli/v2 v2.1.1
	golang.org/x/sys v0.0.0-20200323222414-85ca7c5b95cd // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
